#!/bin/sh

set -e

VERSION="${VERSION-20.04}"
RELEASE="${RELEASE-ubuntu-20.04.2-live-server-amd64}"
DISK_IMG="${DISK_IMG-$RELEASE.qcow2}"
SNAPSHOT_IMG="${SNAPSHOT_IMG-$RELEASE.snapshot.qcow2}"
DEV_IMG="${DEV_IMG-nvm.qcow2}"
ISO="${ISO-$RELEASE.iso}"

if [ ! -f "$ISO" ]; then
	wget "https://releases.ubuntu.com/$VERSION/$ISO"
fi

if [ ! -f "$DISK_IMG" ]; then
	qemu-img create -f qcow2 "$DISK_IMG" 20G
	qemu-system-x86_64 \
		-cdrom "$ISO" \
		-drive "file=$DISK_IMG,format=qcow2" \
		-enable-kvm \
		-m 2G \
		-smp 2
fi

if [ ! -f "$SNAPSHOT_IMG" ]; then
	qemu-img create \
		-b "$DISK_IMG" \
		-f qcow2 \
		"$SNAPSHOT_IMG"
fi

if [ ! -f "$DEV_IMG" ]; then
	qemu-img create "$DEV_IMG" 20G
fi

qemu-system-x86_64 \
	-drive "file=$SNAPSHOT_IMG,format=qcow2" \
	-enable-kvm \
	-m 2G \
	-smp 2 \
	-device intel-hda \
	-device hda-duplex \
	-vga virtio \
	-device nvme-subsys,id=nvme-subsys-0,nqn=subsys0 \
	-drive "file=$DEV_IMG,format=raw,if=none,id=nvm" \
	-device nvme,serial=foobar,subsys=nvme-subsys-0 \
	-device nvme-ns,drive=nvm,nsid=1 \
	"$@"
